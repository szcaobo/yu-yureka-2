#!/usr/bin/perl
#####################################################################################################################
#
#将repo log 转换成excel格式,传入参数，第一个生成的logtxt文件，第二个参数输出的excel文件名
#####################################################################################################################
BEGIN{push @INC,'./android/tools/wingtechtools/perl_pm'};
use Spreadsheet::WriteExcel;
use Encode;


if(scalar(@ARGV) > 1)
{
    $file_name = $ARGV[0];
    $excel_file= $ARGV[1];
}
else
{
    print "Please enter log.txt and log.xml";
    return 1;
}

open (LOGS,"<$file_name") or die "can't open log file $file_name!";

my @repologs=<LOGS>;

close(LOGS);

open (OUT,">$excel_file") or die "cant't open out file $excel_file!";

print "Convert $file_name to $excel_file...\n";

my $project=undef;
my $old_project=undef;
my $commit=undef;
my $author=undef;
my $date=undef;
my $content=undef;

my @colhead = ('模块(project)', 'commit', '修改人', '修改时间','修改说明');
my @colwidth = (12, 20, 12, 12,80);
$xls_wb = Spreadsheet::WriteExcel->new($excel_file);
$xls_ws = $xls_wb->add_worksheet();

my $normalFormat = $xls_wb->add_format(border => 1,align => "center",size=>10);
my $normalFormat2 = $xls_wb->add_format(border => 1,align => "center",size=>10);
my $titleFormat = $xls_wb->add_format(bg_color => "yellow",border => 1,align=>"center",size=>12);
my $tuchuFormat = $xls_wb->add_format(border => 1,align=>"left",size=>12);
$normalFormat2->set_bg_color('27');
$titleFormat->set_bold();

my $col = 0;
my $row = 1;
foreach (@colhead)
{

    $xls_ws->write_utf16be_string(0, $col, Encode::encode("utf16be",Encode::decode("gb2312",$colhead[$col])), $titleFormat);
    $xls_ws->set_column($col, $col, $colwidth[$col]);
    $col++;
}




foreach $line(@repologs){	
	
# 去掉空白行
	if($line =~ /^\s*$/){
		next;
	}
	#去掉前后空白
	chomp($line);
	#替换特殊字符

   $line =~ s/</&lt;/;
   $line =~ s/>/&gt;/;


	#如果匹配project/，写入第一列
	if($line =~ /project\s+([0-9a-zA-Z\/.]*)$/){
		$old_project = $project;
		$project=$1;		
#		print "project=$project\n";	
	}	

	#如果匹配commit
	elsif($line=~ /commit\s+([0-9a-zA-Z\/]*)$/g){	
		#新加一行
		if($commit ne ""){
			if ($row % 2 == 0)
      {
      	$fotmat = $normalFormat;
      }
      else
      {
      	$fotmat = $normalFormat2;
      }
			$xls_ws->write_utf16be_string($row, 0,Encode::encode("utf16be",Encode::decode("utf8",$old_project)), $fotmat);
			$xls_ws->write_utf16be_string($row, 1,Encode::encode("utf16be",Encode::decode("utf8",$commit)), $fotmat);
			$xls_ws->write_utf16be_string($row, 2,Encode::encode("utf16be",Encode::decode("utf8",$author)), $fotmat);
			$xls_ws->write_utf16be_string($row, 3,Encode::encode("utf16be",Encode::decode("utf8",$date)), $fotmat);
			$xls_ws->write_utf16be_string($row, 4,Encode::encode("utf16be",Encode::decode("utf8",$content)), $fotmat);
      $row++;
		}
		$commit=$1;	
		$content=undef;
		$old_project = $project;
	}	
	
	#如果匹配author:
	elsif($line=~/Author:\s*(\w+).+gt;$/){
		$author=$1;		
#		print "author=$author";
		}
	#如果匹配Date:
	elsif($line=~/Date:\s*([0-9a-zA-Z\/:+\- ]*)$/g){
		$date=$1;		
		}		
	#如果什么都不匹配，也不是完全空白
	else{
		$line =~ s/^\s+//g;
		$content.=$line;
	}

}
	
#最后一行
  if ($row % 2 == 0)
  {
     $fotmat = $normalFormat;
  }
  else
  {
     $fotmat = $normalFormat2;
  }
	$xls_ws->write_utf16be_string($row, 0,Encode::encode("utf16be",Encode::decode("utf8",$old_project)), $fotmat);
	$xls_ws->write_utf16be_string($row, 1,Encode::encode("utf16be",Encode::decode("utf8",$commit)), $fotmat);
	$xls_ws->write_utf16be_string($row, 2,Encode::encode("utf16be",Encode::decode("utf8",$author)), $fotmat);
	$xls_ws->write_utf16be_string($row, 3,Encode::encode("utf16be",Encode::decode("utf8",$date)), $fotmat);
	$xls_ws->write_utf16be_string($row, 4,Encode::encode("utf16be",Encode::decode("utf8",$content)), $fotmat);
  $row++;


$xls_ws->autofilter(0, 0, 0,0, 3);
$xls_wb->close();
