#!/usr/bin/env python
# _*_ coding:utf-8 _*_

import os
import commands
import sys

def wt_get(cmd):
    return commands.getstatusoutput(cmd)[1]
    
def usage():
    print "usage:  ./change_str1_to_str2.py config_file src_file bak_file"
    print "config_file must contain lines like boot.img:B3I100EUOD000.MBN"
    sys.exit(-1)
    
if __name__ == '__main__':
    if not len(sys.argv) ==4:
        usage()
    config_file=sys.argv[1]
    src_file=sys.argv[2]
    bak_file=sys.argv[3]
   
    if os.path.isfile(config_file) and os.path.isfile(src_file):
        pass
    else:
        print "not exist file \n   %s\nor %s \n please check !!! "%(config_file,src_file)
        sys.exit(-1)
    if len(os.path.dirname(bak_file)) > 0:
        os.system("mkdir -p %s"%(os.path.dirname(bak_file)))
    os.system("cp %s %s"%(src_file,bak_file))
    f=open(config_file,'r')
    for line in f: 
        if ':' in line and not line.strip().startswith("#") and not line.strip().startswith("//"):
            cmd = 'sed -i "s/%s/%s/" %s'%(line.strip().split(':')[0],line.strip().split(':')[1],src_file)
            os.system(cmd)
    f.close()
    
