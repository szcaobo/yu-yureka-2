#!/usr/bin/env python
import os
import sys
import time
import xlrd
import xlwt
import commands
import getpass
import re
import xmlrpclib
sys.path.insert(1,'./android/tools/wingtechtools/python/')

def get_branch():
    branch_list = []
    cmd = "cd android/frameworks/base;git branch -a;cd -"
    out = commands.getstatusoutput(cmd)[1]
    for one in out.split():
        if one.startswith("remotes/aosp/"):
            branch_list.append(one[13:])
        if one.startswith("aosp/"):
            current_branch = one[5:]
    br = raw_input("Remote branch        [%s/?]: "%current_branch)
    if br == "":
        br=current_branch
    elif br == "?":
        print "The remote branch list as:"
        for one in branch_list:
            print "\t"+one
        return get_branch()
    else:
        if br not in branch_list:
            print "Sorry, try again!"
            return get_branch()
    return br
     
def get_bugzilla_account():
    try:
        f = open("/home/%s/.autofixbugconfig"%USER)
        lines=f.readlines()
        for one in lines:
            one = one.strip()
            if "bugzilla_account=" in one:
                account = one.split("=")[1]
            if "bugzilla_passwd=" in one:
                passwd = one.split("=")[1]
    except Exception:
        account = ""
        passwd = ""
    return account,passwd
   
def get_email():
    default_email = "%s@wingtech.net"%USER
    t1 = raw_input("Bugzilla Account       [%s]: "%default_email)
    if t1 == "":
        t1 = default_email
    return t1

def get_passwd():
    t1  = getpass.getpass('password: ')
    if t1 == "exit":sys.exit(-1)
    try:
        server = xmlrpclib.ServerProxy("http://192.168.2.89/xmlrpc.cgi")
        dl = server.User.login({"login":"%s"%bugzilla_account,"password":"%s"%t1})
    except:
        print "Sorry, try again!"
        return get_passwd()
    return t1

def onremember_password():
    t1 = raw_input("Remember the password          [Y/n]: ")
    if t1 == "":
        t1 = "Y"
    if t1 == "Y":
        f=open("/home/%s/.autofixbugconfig"%USER,"w")
        f.write("bugzilla_account=%s\n"%bugzilla_account)
        f.write("bugzilla_passwd=%s"%bugzilla_passwd)
        f.close()

def get_time1():
    t1 = raw_input("Date from            [2015-0-01]: ")
    if t1 == "":
        t1 = "2015-01-01"
    elif t1 < "2015-01-01" or t1 > "9999-12-31":
        print "Sorry, try again!"
        return get_time1()
    return t1

def get_time2():
    t2 = raw_input("Date to              [9999-12-31]: ")
    if t2 == "":
        t2 = "9999-12-31"
    elif t2 < "2015-01-01" or t2 > "9999-12-31":
        print "Sorry, try again!"
        return get_time2()
    return t2


def generate():
    lists = []
    a1 = '--pretty=format:"%s"'
    cmd = "repo forall -pc git log --since=%s --before=%s --grep=[Bb][Uu][Gg] %s"%(begin_time,end_time,a1)
    os.system("%s > .repo/output.log"%cmd)
    f = open(".repo/output.log")
    lines = f.readlines()
    for one in lines:
        try:
            pp = "[Bb][uU][gG](( )*\d+)+"
            m = re.search(pp,one)
            out = m.group()
        except Exception:
            out = ""
        if out != "":
            if out[3] == " ":
                if " " not in out[4:]:
                    lists.append(out[4:])
                else:
                    for two in out[4:].split(" "):
                        lists.append(two)
            else:
                if " " not in out[3:]:
                    lists.append(out[3:])
                else:
                    for two in out[3:].split(" "):
                        lists.append(two)
    lists = list(set(lists))
    lists.sort()
    total_number = len(lists)
    server = xmlrpclib.ServerProxy("http://192.168.2.89/xmlrpc.cgi")
    dl = server.User.login({"login":bugzilla_account,"password":bugzilla_passwd})
    tk = dl.values()[0]
    print "\n......\n"
    print "A total of %d records, begin to check the status on bugzilla!"%total_number
    lists2 = []
    i = 1
    for one in lists:
        try:
            ori_status = server.Bug.get({"ids":one,'token':tk}).values()[1][0].values()[-3]
        except Exception:
            ori_status = ""
        print "[%d / %d] %s : %s"%(i,total_number,one,ori_status)
        i = i + 1
        if ori_status == "CODECOMM":
            lists2.append(one)
    print "\n===============================================\n"
    print "Bugs CODECOMM: %d ! They are:"%len(lists2)
    print lists2
    return lists2
    
def autofix(lists):
    server = xmlrpclib.ServerProxy("http://192.168.2.89/xmlrpc.cgi")
    dl = server.User.login({"login":bugzilla_account,"password":bugzilla_passwd})
    tk = dl.values()[0]
    t1 = raw_input("Auto fix on bugzilla        [Y/n]: ")
    if t1 == "":
        t1 = "Y"
    if t1 == "Y":
        comment = raw_input("Additional Comments       []: ")
        if comment == "":
            print "You should enter at least one world as the addtional comments, such as 'for fix version:V011'!"
            comment = raw_input("Additional Comments       []: ")
        if comment == "":
            print "You should enter at least one world as the addtional comments, such as 'for fix version:V011'!"
            comment = raw_input("Additional Comments       []: ")
        print "begin to auto fix......"
        for one in lists:
            server.Bug.update({'ids':one,'comment':{'body':comment},'resolution':'FIXED','token':tk})
            print "Bug %s : from CODECOMM to FIXED successed!"%one
    else:
        sys.exit(-1)

if __name__ == '__main__':
    USER = getpass.getuser()
    print "*** Fix bugs Automatically"
    print "***\n"
    if get_bugzilla_account()[0] == "" or get_bugzilla_account()[1] == "":
        print "\n*** Bugzilla Account"
        print "***\n"
        bugzilla_account = get_email()
        bugzilla_passwd = get_passwd()
        onremember_password()
    else:
        bugzilla_account = get_bugzilla_account()[0]
        bugzilla_passwd = get_bugzilla_account()[1]    
    print "*** Branch"
    print "***\n"
    branch  = get_branch()
    print "\n*** Time"
    print "***\n"
    begin_time = get_time1()
    end_time   = get_time2()
    print "\n"
    print "="*80
    print "Now start to search and generate, waiting please ..."
    print "Branch:  "+branch
    print "From:    "+begin_time
    print "To:      "+end_time
    codecomm_list = generate()
    print "*** Auto Fix"
    print "***\n"
    autofix(codecomm_list)


